import psycopg2
import requests
from . import config
from . import xlogger
from datetime import datetime
from collections import defaultdict
from . import twitch_oauth, structures


def get_new_con():
    con = psycopg2.connect(
        database=config.DB_NAME,
        user=config.DB_USER,
        password=config.DB_PASS,
        host=config.DB_HOST,
        port=config.BD_PORT
    )

    return con


def commit_con(con):
    con.commit()
    con.close()


def update_info():
    users = []
    insert_data = []
    con = get_new_con()
    xlogger.logging.info('get connection in update_info')
    with con.cursor() as cursor:
        cursor.execute('SELECT id, fio, fio_r, channel from twitch_account')
        for row in cursor:
            users.append(structures.TwitchUser(row[0], row[1], row[2], row[3]))
        xlogger.logging.info('select info about user')
    for user in users:
        videos_on_twitch = twitch_oauth.get_new_channel_videos(user._id, limit=20, sort='time')
        with con.cursor() as cursor:
            cursor.execute('SELECT id, status FROM twitch_vod WHERE account_id={}'.format(user._id))
            videos_from_db = [row[0] for row in cursor]
        for twitch_video in videos_on_twitch:
            if int(twitch_video._id) not in videos_from_db:
                insert_data.append('({0}, {1}, \'new\', '
                                   '\'{2}\', \'{3}\', {4})'.format(twitch_video._id, user._id,
                                           twitch_video.title, twitch_video.published_at,
                                           twitch_video.length))
    if insert_data:
        psql_request = 'INSERT INTO twitch_vod (id, account_id, status, name, published_at, length) values ' + ', '.join(insert_data)
        with con.cursor() as cursor:
            cursor.execute(psql_request)
    else:
        pass
    commit_con(con)


def check_videos_length():
    con = get_new_con()
    videos = []
    with con.cursor() as cursor:
        cursor.execute('SELECT id, length from twitch_vod where status=\'new\'')
        for row in cursor:
            videos.append((row[0], row[1]))
    commit_con(con)
    updated_info = []
    for video_id, length in videos:
        try:
            cur_length = twitch_oauth.get_video(video_id).length
            updated_info.append((video_id, cur_length))
        except requests.exceptions.HTTPError as err:
            change_status_twitch_vod([video_id], 'cant_download')
            xlogger.logging.error(err, exc_info=True)
    for video_id, length in updated_info:
        con = get_new_con()
        with con.cursor() as cursor:
            cursor.execute('UPDATE twitch_vod SET length={0} WHERE id={1}'.format(length, video_id))
        commit_con(con)


def get_new_videos(date=''):
    if not date:
        date = '\'%\''
    else:
        date = '\'' + date + '%\''
    data = {}
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('SELECT id, fio, fio_r, channel from twitch_account')
        for row in cursor:
            user = structures.TwitchUser(row[0], row[1], row[2], row[3])
            data[user] = []
    excess_users = []
    for user in data.keys():
        f = True
        with con.cursor() as cursor:
            cursor.execute('SELECT id, name, published_at, length from twitch_vod WHERE account_id={0} '
                           'AND status=\'new\' AND published_at like {1}'.format(user._id, date))
            for row in cursor:
                f = False
                data[user].append(structures.TwitchVideo(row[0], row[3], row[1], row[2].replace('T', ' ').replace('Z', '')))
        if f:
            excess_users.append(user)
        data[user] = sorted(data[user], key=lambda x: datetime.strptime(x.published_at, '%Y-%m-%d %H:%M:%S'),
                            reverse=True)
    for user in excess_users:
        data.pop(user)
    commit_con(con)
    return data


def get_videos_for_download():
    con = get_new_con()
    data = {}
    aux_data = []
    with con.cursor() as cursor:
        cursor.execute('SELECT id, account_id, published_at, length from twitch_vod where status=\'to_download\'')
        for row in cursor:
            publ = row[2][:row[2].index('T'):]
            aux_data.append([row[0], row[1], publ, row[3]])
            data[publ] = defaultdict(list)
    for video_id, account_id, publ, length in aux_data:
        data[publ][account_id].append((video_id, length))
    commit_con(con)
    return data


def change_status_twitch_vod(videos, status):
    for video in videos:
        con = get_new_con()
        with con.cursor() as cursor:
            cursor.execute('UPDATE twitch_vod SET status=\'{0}\' WHERE id={1}'.format(status, video))
        commit_con(con)


def get_videos_waiting_for_info():
    data = []
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('SELECT id from twitch_vod WHERE status=\'waiting_for_info\'')
        for row in cursor:
            data.append(row[0])
    commit_con(con)
    return data


def get_videos_for_concat():
    con = get_new_con()
    aux_data = []
    with con.cursor() as cursor:
        cursor.execute('SELECT id, account_id, published_at from twitch_vod where status=\'to_concat\'')
        for row in cursor:
            aux_data.append((row[0], row[2].replace('T', ' ').replace('Z', ''), '%Y-%m-%d %H:%M:%S'))
            account = row[1]
    data = sorted(aux_data, key=lambda x: x[1])
    print(data)
    date = str(data[0][1])[:str(data[0][1]).index(' ')]
    return account, date, [video_id[0] for video_id in data]


def update_titles_statuses(title, description):
    con = get_new_con()
    videos_id = get_videos_waiting_for_info()
    with con.cursor() as cursor:
        cursor.execute('SELECT title, description from current_youtube_info')
        for row in cursor:
            c_title, c_description = row[0], row[1]
    title = title if title else c_title
    description = description if description else c_description
    req = 'UPDATE current_youtube_info SET (title, description) =(\'{0}\', \'{1}\')'.format(title, description)
    with con.cursor() as cursor:
        cursor.execute(req)
    commit_con(con)
    change_status_twitch_vod(videos_id, 'to_download')


def twitch_vod_drop_line(video_id):
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('UPDATE twitch_vod SET status=\'deleted_from_twitch\' WHERE id={0}'.format(video_id))
    commit_con(con)


def get_current_youtube_info():
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('SELECT title, description from current_youtube_info')
        for row in cursor:
            title, description = row[0], row[1]
    commit_con(con)
    return title, description


def insert_vod_concat(video_id, title, description, video_path):
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('SELECT account_id from twitch_vod WHERE id={0}'.format(video_id))
        for row in cursor:
            account_id = row[0]
    with con.cursor() as cursor:
        cursor.execute('SELECT fio, fio_r, channel from twitch_account WHERE id={0}'.format(account_id))
        for row in cursor:
            name, namer, channel = row[0], row[1], config.TWITCH_COMMON_LINK + row[2]
    new_title = title.replace('%fio%', name).replace('%fior%', namer).replace('%twitch%', channel)
    new_description = description.replace('%fio%', name).replace('%fior%', namer).replace('%twitch%', channel)
    with con.cursor() as cursor:
        cursor.execute('INSERT INTO vod_concat (id, youtube_title, youtube_description, youtube_concat_pre, status)'
                       ' values ({0}, \'{1}\', \'{2}\', \'{3}\','
                       ' \'to_upload\')'.format(video_id, new_title, new_description, video_path))
    commit_con(con)


def insert_upload_status(video_id):
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('INSERT INTO upload_status (id, total, progress) values ({0}, 0, 100)'.format(video_id))
    commit_con(con)


def update_upload_status(video_id, progress, total):
    con = get_new_con()
    p = progress / total * 1000
    with con.cursor() as cursor:
        cursor.execute('UPDATE upload_status SET total = 1000, progress={0} WHERE id={1}'.format(p, video_id))
    commit_con(con)


def drop_from_upload_status(video_id):
    con = get_new_con()
    with con.cursor() as cursor:
        cursor.execute('DELETE from upload_status WHERE id={0}'.format(video_id))
    commit_con(con)


