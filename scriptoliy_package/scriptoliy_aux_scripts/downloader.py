import re
import os
import m3u8
import shutil
import time
from . import config
import requests
from . import xlogger
import tempfile
import subprocess
from . import twitch_oauth

from pathlib import Path
from . import data_base_interaction
from functools import partial
from datetime import datetime
from urllib.parse import urlparse
from collections import OrderedDict
from concurrent.futures import ThreadPoolExecutor, as_completed


CHUNK_SIZE = 1024
BYTERATE_CONST = 294167.392535
CONNECT_TIMEOUT = 5
RETRY_COUNT = 5
DOWN_DIR = config.DOWN_DIR
FILES_FOR_CONCAT = config.FILES_FOR_CONCAT


def _join_vods(playlist_path, target):
    command = [
        'ffmpeg',
        '-i', playlist_path,
        '-c', 'copy',
        target,
        '-stats',
        '-loglevel', 'warning',
    ]
    result = subprocess.run(command)


def _video_target_filename(video, format):
    return DOWN_DIR + video._id + '.' + format


def _parse_playlists(playlists_m3u8):
    playlists = m3u8.loads(playlists_m3u8)
    for p in playlists.playlists:
        name = p.media[0].name if p.media else ''
        resolution = 'x'.join(str(r) for r in p.stream_info.resolution)
        yield name, resolution, p.uri


def _get_playlist_by_name(playlists, quality):
    for name, _, uri in playlists:
        if name == quality:
            return uri


def _get_vod_paths(playlist, start, end):
    """Extract unique VOD paths for download from playlist."""
    files = []
    vod_start = 0
    for segment in playlist.segments:
        vod_end = vod_start + segment.duration
        start_condition = not start or vod_end > start
        end_condition = not end or vod_start < end

        if start_condition and end_condition and segment.uri not in files:
            files.append(segment.uri)

        vod_start = vod_end

    return files


def _crete_temp_dir(base_uri):
    """Create a temp dir to store downloads if it doesn't exist."""
    path = urlparse(base_uri).path.lstrip('/')
    temp_dir = Path(tempfile.gettempdir(), 'twitch-dl', path)
    temp_dir.mkdir(parents=True, exist_ok=True)
    return temp_dir


def _select_playlist_interactive(playlists):
    _, _, uri = playlists[0]
    return uri


def download(args):
    file_names = []
    xlogger.logging.info('Starting download with args: {0}'.format(args))
    for arg in args:
        log_str = str(arg)
        xlogger.logging.info('DOWNLOADING>>> {0}'.format(log_str))
        total, _, free = shutil.disk_usage('/')
        bytes_for_video = BYTERATE_CONST * arg.end * 2
        f = True
        any_exc = False
        while f:
            if bytes_for_video < free:
                try:
                    data_base_interaction.insert_upload_status(arg.video)
                    file_names.append('{0}{1}.{2}'.format(DOWN_DIR, arg.video, arg.format))
                    data_base_interaction.change_status_twitch_vod([arg.video], 'downloading')

                    _download_video(arg.video, arg)
                    data_base_interaction.drop_from_upload_status(arg.video)
                    data_base_interaction.change_status_twitch_vod([arg.video], 'to_concat')
                except Exception as err:
                    any_exc = True
                    xlogger.logging.error(err, exc_info=True)
                    data_base_interaction.change_status_twitch_vod([arg.video], 'cant_download')
                f = False
            elif bytes_for_video > total:
                any_exc = True
                data_base_interaction.change_status_twitch_vod([arg.video], 'cant_download')
                f = False
            else:
                xlogger.logging.info('Time sleep in download with args: {0}'.format(log_str))
                time.sleep(20)
    if (not any_exc):
        concat_downloaded_videos(args[0].title, args[0].description)


def concat_downloaded_videos(title, description):
    # (user_id, video_id, date)
    xlogger.logging.info('Concatenating')
    user_id, date, videos = data_base_interaction.get_videos_for_concat()
    data_base_interaction.change_status_twitch_vod(videos, 'concatenating')
    if len(videos) > 1:
        dump_file_names(videos)
        command = [
            'ffmpeg', '-f', 'concat', '-safe', '0',
            '-i', FILES_FOR_CONCAT,
            '-c', 'copy',
             '{0}{1}_{2}.mp4'.format(DOWN_DIR, user_id, date),
             '-stats',
            '-loglevel', 'warning',
        ]
        result = subprocess.run(command)
        for video in videos:
            os.remove('{0}{1}.mp4'.format(DOWN_DIR, video))
    else:
        os.rename('{0}{1}.mp4'.format(DOWN_DIR, videos[0]), '{0}{1}_{2}.mp4'.format(DOWN_DIR, user_id, date))
    data_base_interaction.change_status_twitch_vod(videos, 'concatenated')
    data_base_interaction.insert_vod_concat(videos[0], title, description, '{0}{1}_{2}.mp4'.format(DOWN_DIR, user_id, date))


def _download_video(video_id, args):
    video = twitch_oauth.get_video(video_id)
    access_token = twitch_oauth.get_access_token(video_id)

    playlists_m3u8 = twitch_oauth.get_playlists(video_id, access_token)
    playlists = list(_parse_playlists(playlists_m3u8))
    playlist_uri = _select_playlist_interactive(playlists)

    response = requests.get(playlist_uri)
    response.raise_for_status()
    playlist = m3u8.loads(response.text)

    base_uri = re.sub('/[^/]+$', '/', playlist_uri)
    target_dir = _crete_temp_dir(base_uri)
    vod_paths = _get_vod_paths(playlist, args.start, args.end)

        # Save playlists for debugging purposes
    with open(os.path.join(str(target_dir), 'playlists.m3u8'), "w") as f:
        f.write(playlists_m3u8)
    with open(os.path.join(str(target_dir), 'playlist.m3u8'), "w") as f:
        f.write(response.text)

    path_map = download_files(base_uri, target_dir, vod_paths, args.max_workers, args.video)

    org_segments = playlist.segments.copy()
    playlist.segments.clear()
    for segment in org_segments:
        if segment.uri in path_map:
            segment.uri = path_map[segment.uri]
            playlist.segments.append(segment)

    playlist_path = os.path.join(str(target_dir), 'playlist_downloaded.m3u8')
    playlist.dump(playlist_path)

    target = _video_target_filename(video, args.format)
    _join_vods(playlist_path, target)
    shutil.rmtree(str(target_dir))
    os.rmdir('/'.join(str(target_dir).split('/')[0:4]))

    xlogger.logging.info('Downloaded: {0}'.format(target))


def _download(url, path):
    tmp_path = path + '.tmp'
    response = requests.get(url, stream=True, timeout=CONNECT_TIMEOUT)
    size = 0
    with open(tmp_path, 'wb') as target:
        for chunk in response.iter_content(chunk_size=CHUNK_SIZE):
            target.write(chunk)
            size += len(chunk)

    os.rename(tmp_path, path)
    return size


def download_file(url, path, retries=RETRY_COUNT):
    if os.path.exists(path):
        return os.path.getsize(path)

    for _ in range(retries):
        try:
            return _download(url, path)
        except:
            time.sleep(10)


def _format_size(value, digits, unit):
    if digits > 0:
        return '{{:.{}f}}{}'.format(digits, unit).format(value)
    else:
        return '{{:d}}{}'.format(unit).format(value)


def format_size(bytes_, digits=1):
    if bytes_ < 1024:
        return _format_size(bytes_, digits, 'B')

    kilo = bytes_ / 1024
    if kilo < 1024:
        return _format_size(kilo, digits, 'kB')

    mega = kilo / 1024
    if mega < 1024:
        return _format_size(mega, digits, 'MB')

    return _format_size(mega / 1024, digits, 'GB')


def format_duration(total_seconds):
    total_seconds = int(total_seconds)
    hours = total_seconds // 3600
    remainder = total_seconds % 3600
    minutes = remainder // 60
    seconds = total_seconds % 60

    if hours:
        return '{} h {} min'.format(hours, minutes)

    if minutes:
        return '{} min {} sec'.format(minutes, seconds)

    return '{} sec'.format(seconds)


def _print_progress(futures, video_id):
    downloaded_count = 0
    downloaded_size = 0
    max_msg_size = 0
    start_time = datetime.now()
    total_count = len(futures)

    try:
        for future in as_completed(futures):
            size = future.result()
            downloaded_count += 1
            downloaded_size += size

            percentage = 100 * downloaded_count // total_count
            est_total_size = int(total_count * downloaded_size / downloaded_count)
            duration = (datetime.now() - start_time).seconds
            speed = downloaded_size // duration if duration else 0
            remaining = (total_count - downloaded_count) * duration / downloaded_count

            data_base_interaction.update_upload_status(video_id, downloaded_size, est_total_size)
            msg = ' '.join([
                'Downloaded VOD {}/{}'.format(downloaded_count, total_count),
                '({}%)'.format(percentage),
                '{}'.format(format_size(downloaded_size)),
                'of ~{}'.format(format_size(est_total_size)),
                'at {}/s'.format(format_size(speed)) if speed > 0 else '',
                'time {}'.format(datetime.now()),
            ])

            max_msg_size = max(len(msg), max_msg_size)
    except Exception as err:
        xlogger.logging.error(err, exc_info=True)


def dump_file_names(videos):
    with open(FILES_FOR_CONCAT, 'w') as f:
        for video in videos:
            f.write('file \'{0}{1}.mp4\'\n'.format(DOWN_DIR, video))


def download_files(base_url, target_dir, vod_paths, max_workers, video_id):
    """
    Downloads a list of VODs defined by a common `base_url` and a list of
    `vod_paths`, returning a dict which maps the paths to the downloaded files.
    """
    urls = [base_url + path for path in vod_paths]
    targets = [os.path.join(str(target_dir), '{:05d}.ts'.format(k)) for k, _ in enumerate(vod_paths)]
    partials = [partial(download_file, url, path) for url, path in zip(urls, targets)]
    xlogger.logging.info('Partials in download_files: {0}'.format(len(partials)))

    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        futures = [executor.submit(fn) for fn in partials]
        _print_progress(futures, video_id)

    return OrderedDict(zip(vod_paths, targets))
