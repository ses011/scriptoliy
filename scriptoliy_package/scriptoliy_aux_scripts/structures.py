import datetime


class ArgsForDownload:
    def __init__(self, video, max_workers, start, end, title=None, description=None):
        self.video = video
        self.max_workers = max_workers
        self.start = start
        self.end = end
        self.format = 'mp4'
        self.keep = False
        self.quality = None
        self.title = title or ''
        self.description = description or ''

    def __str__(self):
        return 'Args ({0}, {1})'.format(self.video, self.end)


class TwitchUser:
    def __init__(self, _id, fio, fior, channel):
        self.channel = channel
        self.fio = fio
        self.fior = fior
        self._id = _id

    def __str__(self):
        return '(TwitchUser: {0}, {1}, {2})'.format(self._id, self.channel, self.fio)


class TwitchVideo:
    def __init__(self, _id, length, title, publ):
        self._id = _id
        self.length = length
        self.title = title
        self.published_at = publ

    def get_formatted_length(self):
        return str(datetime.timedelta(seconds=self.length))

    def __str__(self):
        return '(TwitchVideo: {0},' \
               ' {1}, ' \
               '{2})'.format(self._id, self.title, self.length)
