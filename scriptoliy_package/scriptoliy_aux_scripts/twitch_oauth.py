import requests
from . import config
from .structures import TwitchUser, TwitchVideo


BASE_URL_KRAKEN = config.TWITCH_BASE_URL_KRAKEN
ACCEPT = config.TWITCH_ACCEPT


def authenticated_get(url, client_id, params={}, headers={}):
    headers['Client-ID'] = client_id
    response = requests.get(url, params, headers=headers)
    response.raise_for_status()
    return response


def authenticated_post(url, client_id, data=None, json=None, headers={}):
    headers['Client-ID'] = client_id
    response = requests.post(url, data=data, json=json, headers=headers)
    response.raise_for_status()
    return response


def kraken_get(url, client_id, params={}, headers={}):
    headers['Accept'] = ACCEPT
    return authenticated_get(url, client_id, params, headers)


def get_video(video_id):
    client_id = config.TWITCH_CLIENT_ID_UPDATE
    url = BASE_URL_KRAKEN + 'videos/{0}'.format(video_id)
    resp = kraken_get(url, client_id).json()
    res = TwitchVideo(resp['_id'][1:],
                      resp['length'],
                      resp['title'],
                      resp['published_at'])
    return res


def get_channel_info(channel_name):
    client_id = config.TWITCH_CLIENT_ID_UPDATE
    url = BASE_URL_KRAKEN + 'search/channels?query={0}'.format(channel_name)
    resp = kraken_get(url, client_id).json()['channels'][0]
    res = None
    if resp:
        res = TwitchUser(resp['_id'], resp['name'], channel_name, channel_name)
    return res


def get_new_channel_videos(channel_id, limit, sort, _type='archive', offset=0):
    client_id = config.TWITCH_CLIENT_ID_UPDATE
    params = {
        'limit': limit,
        'offset': offset,
        'broadcast_type': _type,
        'sort': sort,
    }
    resp = kraken_get(BASE_URL_KRAKEN + 'channels/{0}/videos'.format(channel_id), client_id, params=params).json()['videos']
    res = []
    for video in resp:
        res.append(TwitchVideo(video['_id'][1:],
                               video['length'],
                               video['title'],
                               video['published_at']))
    return res


def get_access_token(video_id):
    client_id = config.TWITCH_CLIENT_ID_DOWNLOAD
    url = 'https://api.twitch.tv/api/vods/{}/access_token'.format(video_id)
    return authenticated_get(url, client_id).json()


def get_playlists(video_id, access_token):
    """
    For a given video return a playlist which contains possible video qualities.
    """
    url = 'http://usher.twitch.tv/vod/{}'.format(video_id)

    response = requests.get(url, params={
        'nauth': access_token['token'],
        'nauthsig': access_token['sig'],
        'allow_source': 'true',
        'player': 'twitchweb',
    })
    response.raise_for_status()
    return response.content.decode('utf-8')

