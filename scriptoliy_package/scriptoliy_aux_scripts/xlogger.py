import logging
from . import config

logging.basicConfig(level=logging.INFO, filename=config.LOG_PATH, format='%(asctime)s %(levelname)s:%(message)s')