import threading
import time
from scriptoliy_aux_scripts import downloader, data_base_interaction, structures, xlogger, config


class WorkerDownloader(threading.Thread):
    def __init__(self):
        super().__init__()

    def run(self):
        while True:
            resp = data_base_interaction.get_videos_for_download()
            title, description = data_base_interaction.get_current_youtube_info()
            for date in resp.keys():
                for user in resp[date]:
                    args_for_download = self.__get_args_for_download(resp[date][user], title, description)
                    downloader.download(args_for_download)
            time.sleep(30)

    @staticmethod
    def __get_args_for_download(videos, title, description):
        res = []
        for video, length in videos:
            res.append(structures.ArgsForDownload(video, 20, start=0, end=length, title=title, description=description))
        return res


def crawler_daemon():
    worker_downloader = WorkerDownloader()
    worker_downloader.daemon = True
    worker_downloader.start()
    xlogger.logging.info('worker-downloader started')
    while True:
        xlogger.logging.info('checking for new videos')
        data_base_interaction.update_info()
        xlogger.logging.info('data base updated with new videos')
        time.sleep(14400)
        data_base_interaction.check_videos_length()
        xlogger.logging.info('changed video length')
        time.sleep(14400)


crawler_daemon()
