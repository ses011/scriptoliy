import datetime
from flask import render_template, request, url_for, redirect
from app import app
from scriptoliy_aux_scripts import data_base_interaction
from collections import defaultdict
from urllib.parse import urlparse


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form.get('submit_button') == 'Принять':
            # get response
            resp = request.form.getlist('twitch_video')
            parsed_resp = defaultdict(list)
            # parse response
            for data in resp:
                ndata = parse_html_response(data)
                parsed_resp[ndata[0]].append(ndata[1])
            for key in parsed_resp.keys():
                 data_base_interaction.change_status_twitch_vod(parsed_resp[key], 'waiting_for_info')
            return redirect(url_for('additional_info'))
        elif request.form.get('date_button') == 'Показать':
            date = request.form.get('selectedDate')
            data = data_base_interaction.get_new_videos(date)
            date = date if date else 'все время'
            return render_template("index.html", data=data, date=date)
        elif request.form.get('update_button') == 'Обновить':
            data_base_interaction.update_info()
            data = data_base_interaction.get_new_videos()
            return render_template('index.html', data=data, date='все время')
        return render_template('Jopa.html')
    elif request.method == 'GET':
        # extract users from data base
        data = data_base_interaction.get_new_videos()
        return render_template("index.html", data=data, date='все время')
    return render_template('Jopa.html')


@app.route('/additional_info', methods=['GET', 'POST'])
def additional_info():
    if request.method == 'POST':
        if request.form.get('submit_button') == 'Начать загрузку':
            resp_t = request.form.get('title')
            resp_d = request.form.get('description')
            data_base_interaction.update_titles_statuses(resp_t, resp_d)
        full_url = request.remote_addr
        parsed_url = urlparse(full_url)
        return redirect('http://{}/uploader/index.html'.format(parsed_url.netloc))
    elif request.method == 'GET':
        return render_template("additional_info.html", data=data_base_interaction.get_current_youtube_info())
    return render_template('Jopa.html')


def parse_html_response(data):
    data = data.replace(')', '')
    data = data.replace('(', '')
    data = data.replace(',', '')
    data = data.replace('\'', '')
    return data.split()
